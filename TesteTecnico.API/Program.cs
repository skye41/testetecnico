using TesteTecnico.Data.Empresa;
using TesteTecnico.Data.Login;
using TesteTecnico.Data.SQLiteDataBase;
using TesteTecnico.Servicos.Empresa;
using TesteTecnico.Servicos.Login;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

#region DependencyInjection
builder.Services.AddSingleton<ISQLiteDB, SQLiteDB>(); 

builder.Services.AddScoped<ILoginServico, LoginServico>();
builder.Services.AddScoped<IEmpresaServico, EmpresaServico>();

builder.Services.AddScoped<ILoginDAO, LoginDAO>();
builder.Services.AddScoped<IEmpresaDAO, EmpresaDAO>();
#endregion

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
