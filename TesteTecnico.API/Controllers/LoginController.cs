﻿using Microsoft.AspNetCore.Mvc;
using TesteTecnico.Dominio.Usuario;
using TesteTecnico.Servicos.Login;

namespace TesteTecnico.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LoginController : Controller
    {
        private readonly ILoginServico _loginServico;
        public LoginController(ILoginServico loginServico)
        {
            _loginServico = loginServico;
        }

        [HttpPost()]
        public async Task<IActionResult> Login([FromBody] UsuarioLogin login)
        {
            try
            {
                return await _loginServico.ValidarLogin(login) ? Ok() : BadRequest("Usuário ou senha inválidos.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Erro ao realizar login, tente novamente mais tarde.");
            }
        }

        [HttpPost("recuperar-senha")]
        public async Task<IActionResult> RecuperarSenha([FromBody] UsuarioLogin login)
        {
            try
            {
                return await _loginServico.RecuperarSenha(login) ? Ok("Sua nova senha foi enviada para o e-mail de recuperação.") : BadRequest("Usuário ou e-mail de recuperação inválidos.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Erro ao recuperar senha, tente novamente mais tarde.");
            }
        }
    }
}
