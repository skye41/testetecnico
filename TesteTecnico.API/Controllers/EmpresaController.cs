﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TesteTecnico.Dominio.Empresa;
using TesteTecnico.Servicos.Empresa;

namespace TesteTecnico.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmpresaController : Controller
    {
        private readonly IEmpresaServico _empresaServico;
        public EmpresaController(IEmpresaServico empresaServico)
        {
            _empresaServico = empresaServico;
        }

        [HttpPost("cadastrar")]
        public async Task<IActionResult> CadastrarEmpresa([FromBody] DadosEmpresa empresa)
        {
            try
            {
                return await _empresaServico.CadastrarEmpresa(empresa) ? Ok() : BadRequest("Dados da empresa inválidos.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Erro ao cadastrar empresa, tente novamente mais tarde.");
            }
        }

        [HttpGet("consultar/{id}")]
        public async Task<IActionResult> ConsultarEmpresa([FromRoute] int id)
        {
            try
            {
                var empresa = await _empresaServico.ConsultarEmpresa(id);

                return empresa != null ? Ok(empresa) : BadRequest("Empresa não encontrada.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Erro ao consultar empresa, tente novamente mais tarde.");
            }
        }

        [HttpPatch("alterar")]
        public async Task<IActionResult> AlterarEmpresa([FromBody] DadosEmpresa empresa)
        {
            try
            {
                return await _empresaServico.AlterarEmpresa(empresa) ? Ok() : BadRequest("Dados da empresa inválidos.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Erro ao alterar dados da empresa, tente novamente mais tarde.");
            }
        }

        [HttpDelete("excluir/{id}")]
        public async Task<IActionResult> ExcluirEmpresa([FromRoute] int id)
        {
            try
            {
                return/* await _empresaServico.CadastrarEmpresa() ? Ok() : */BadRequest("Usuário ou senha inválidos.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Erro ao excluir empresa, tente novamente mais tarde.");
            }
        }

        [HttpGet("listar")]
        public async Task<IActionResult> ListarEmpresas()
        {
            try
            {
                var empresas = await _empresaServico.ListarEmpresas();

                //string json = JsonConvert.SerializeObject(empresas, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore });
                return empresas != null && empresas.Any() ? Ok(empresas) : BadRequest("Nenhuma empresa cadastrada.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Erro ao listar empresas, tente novamente mais tarde.");
            }
        }
    }
}
