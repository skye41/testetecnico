﻿using TesteTecnico.Dominio.Usuario;

namespace TesteTecnico.Servicos.Login
{
    public interface ILoginServico
    {
        Task<bool> ValidarLogin(UsuarioLogin login);

        Task<bool> RecuperarSenha(UsuarioLogin login);
    }
}
