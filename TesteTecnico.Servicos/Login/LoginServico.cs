﻿using TesteTecnico.Data.Login;
using TesteTecnico.Dominio.Usuario;

namespace TesteTecnico.Servicos.Login
{
    public class LoginServico : ILoginServico
    {
        private readonly ILoginDAO _loginDAO;
        public LoginServico(ILoginDAO loginDAO)
        {
            _loginDAO = loginDAO;
        }
        public async Task<bool> ValidarLogin(UsuarioLogin login)
        {
            try
            {
                var usuario = await _loginDAO.ConsultarUsuarioPorLogin(login.Usuario);

                return usuario != null && usuario.Usuario == login.Usuario && usuario.Senha == login.Senha;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<bool> RecuperarSenha(UsuarioLogin login)
        {
            try
            {
                var usuario = await _loginDAO.ConsultarUsuarioPorLogin(login.Usuario);

                if (usuario != null && usuario.Usuario == login.Usuario && usuario.Recuperacao == login.Recuperacao)
                {
                    usuario.Senha = GerarNovaSenha();
                    if(await _loginDAO.AtualizarSenha(usuario))
                    {
                        EnviarEmail(usuario.Senha);
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private string GerarNovaSenha()
        {
            try
            {
                var senha = new Random().Next().ToString();

                if (senha.Length > 10)
                    senha = senha.Substring(0, 10);
                return senha;
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        private void EnviarEmail(string senha)
        {
            try
            {
                // Enviar e-mail
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
