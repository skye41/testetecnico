﻿using TesteTecnico.Dominio.Empresa;

namespace TesteTecnico.Servicos.Empresa
{
    public interface IEmpresaServico
    {
        Task<bool> CadastrarEmpresa(DadosEmpresa empresa);
        Task<DadosEmpresa> ConsultarEmpresa(int id);
        Task<bool> AlterarEmpresa(DadosEmpresa empresa);
        Task<List<DadosEmpresa>> ListarEmpresas();
    }
}
