﻿using TesteTecnico.Data.Empresa;
using TesteTecnico.Dominio.Empresa;

namespace TesteTecnico.Servicos.Empresa
{
    public class EmpresaServico : IEmpresaServico
    {
        private readonly IEmpresaDAO _empresaDAO;
        public EmpresaServico(IEmpresaDAO empresaDAO)
        {
            _empresaDAO = empresaDAO;
        }

        public async Task<bool> CadastrarEmpresa(DadosEmpresa empresa)
        {
            try
            {
                return await _empresaDAO.CadastrarEmpresa(empresa);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<DadosEmpresa> ConsultarEmpresa(int id)
        {
            try
            {
                return await _empresaDAO.ConsultarEmpresaPorId(id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<bool> AlterarEmpresa(DadosEmpresa empresa)
        {
            try
            {
                return await _empresaDAO.AlterarEmpresa(empresa);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<bool> ExcluirEmpresa(DadosEmpresa empresa)
        {
            try
            {
                return await _empresaDAO.CadastrarEmpresa(empresa);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<List<DadosEmpresa>> ListarEmpresas()
        {
            try
            {
                return await _empresaDAO.ListarEmpresas();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
