﻿namespace TesteTecnico.Dominio.Usuario
{
    public class UsuarioLogin : Dominio
    {
        public string Usuario { get; set; }
        public string Senha { get; set; }
        public string Recuperacao { get; set; }
    }
}
