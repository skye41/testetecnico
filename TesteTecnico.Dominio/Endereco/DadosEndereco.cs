﻿namespace TesteTecnico.Dominio.Endereco
{
    public class DadosEndereco : Dominio
    {
        public DadosEndereco()
        {
            Cidade = new Cidade();
        }
        public string Logradouro { get; set; }
        public string NumeroEndereco { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string CEP { get; set; }
        public Cidade Cidade { get; set; }
    }
}
