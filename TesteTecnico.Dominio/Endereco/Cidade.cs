﻿namespace TesteTecnico.Dominio.Endereco
{
    public class Cidade : Dominio
    {
        public Cidade()
        {
            Estado = new Estado();
        }
        public string NomeCidade { get; set; }
        public Estado Estado { get; set; }
    }
}
