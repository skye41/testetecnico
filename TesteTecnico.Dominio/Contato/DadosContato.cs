﻿namespace TesteTecnico.Dominio.Contato
{
    public class DadosContato : Dominio
    {
        public DadosContato()
        {
            TelefoneComercial = new Telefone();
            TelefoneCelular = new Telefone();
            TelefonePrincipal = new Telefone();
        }

        public string Email { get; set; }
        public Telefone TelefonePrincipal { get; set; }
        public Telefone TelefoneCelular { get; set; }
        public Telefone TelefoneComercial { get; set; }
        public string NomeContato { get; set; }
    }
}
