﻿namespace TesteTecnico.Dominio.Contato
{
    public class Telefone : Dominio
    {
        public int DDD { get; set; }
        public string NumeroTelefone { get; set; }
    }
}
