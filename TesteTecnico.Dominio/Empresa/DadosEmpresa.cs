﻿using TesteTecnico.Dominio.Contato;
using TesteTecnico.Dominio.Endereco;

namespace TesteTecnico.Dominio.Empresa
{
    public class DadosEmpresa : Dominio
    {
        public DadosEmpresa()
        {
            Contato = new DadosContato();
            Endereco = new DadosEndereco();
        }

        public string CNPJ { get; set; }
        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }

        public DadosContato Contato { get; set; }
        public DadosEndereco Endereco { get; set; }
    }
}
