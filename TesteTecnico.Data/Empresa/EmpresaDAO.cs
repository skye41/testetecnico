﻿using TesteTecnico.Data.SQLiteDataBase;
using TesteTecnico.Dominio.Contato;
using TesteTecnico.Dominio.Empresa;

namespace TesteTecnico.Data.Empresa
{
    public class EmpresaDAO : IEmpresaDAO
    {
        private readonly ISQLiteDB _db;
        public EmpresaDAO(ISQLiteDB db)
        {
            _db = db;
        }

        public async Task<bool> CadastrarEmpresa(DadosEmpresa empresa)
        {
            using (var conn = _db.GetConnection())
            {
                try
                {
                    using (var cmd = conn.CreateCommand())
                    {
                        #region CONTATO
                        cmd.CommandText = "INSERT INTO Telefone (DDD, Numero) " +
                        "VALUES ($ddd, $numero);";
                        cmd.Parameters.AddWithValue("$ddd", empresa.Contato.TelefonePrincipal.DDD);
                        cmd.Parameters.AddWithValue("$numero", empresa.Contato.TelefonePrincipal.NumeroTelefone);
                        cmd.ExecuteScalar();
                        empresa.Contato.TelefonePrincipal.Id = Convert.ToInt32(conn.LastInsertRowId);

                        cmd.CommandText = "INSERT INTO Telefone (DDD, Numero) " +
                        "VALUES ($ddd, $numero);";
                        cmd.Parameters.AddWithValue("$ddd", empresa.Contato.TelefoneComercial.DDD);
                        cmd.Parameters.AddWithValue("$numero", empresa.Contato.TelefoneComercial.NumeroTelefone);
                        cmd.ExecuteScalar();
                        empresa.Contato.TelefoneComercial.Id = Convert.ToInt32(conn.LastInsertRowId);

                        cmd.CommandText = "INSERT INTO Telefone (DDD, Numero) " +
                        "VALUES ($ddd, $numero);";
                        cmd.Parameters.AddWithValue("$ddd", empresa.Contato.TelefoneCelular.DDD);
                        cmd.Parameters.AddWithValue("$numero", empresa.Contato.TelefoneCelular.NumeroTelefone);
                        cmd.ExecuteScalar();
                        empresa.Contato.TelefoneCelular.Id = Convert.ToInt32(conn.LastInsertRowId);

                        cmd.CommandText = "INSERT INTO Contato (Email, Nome_Contato, Id_Tel_Principal, Id_Tel_Comercial, Id_Tel_Celular) " +
                        "VALUES ($email, $nomeContato, $idPrincipal, $idComercial, $idCelular);";
                        cmd.Parameters.AddWithValue("$email", empresa.Contato.Email);
                        cmd.Parameters.AddWithValue("$nomeContato", empresa.Contato.NomeContato);
                        cmd.Parameters.AddWithValue("$idPrincipal", empresa.Contato.TelefonePrincipal.Id);
                        cmd.Parameters.AddWithValue("$idComercial", empresa.Contato.TelefoneComercial.Id);
                        cmd.Parameters.AddWithValue("$idCelular", empresa.Contato.TelefoneCelular.Id);
                        cmd.ExecuteScalar();
                        empresa.Contato.Id = Convert.ToInt32(conn.LastInsertRowId);
                        #endregion

                        #region ENDERECO
                        cmd.CommandText = "INSERT INTO Endereco (Logradouro, Numero, Complemento, Bairro, CEP, Id_Cidade) " +
                        "VALUES ($logradouro, $numero, $complemento, $bairro, $cep, $cidade);";
                        cmd.Parameters.AddWithValue("$logradouro", empresa.Endereco.Logradouro);
                        cmd.Parameters.AddWithValue("$numero", empresa.Endereco.NumeroEndereco);
                        cmd.Parameters.AddWithValue("$complemento", empresa.Endereco.Complemento);
                        cmd.Parameters.AddWithValue("$bairro", empresa.Endereco.Bairro);
                        cmd.Parameters.AddWithValue("$cep", empresa.Endereco.CEP);
                        cmd.Parameters.AddWithValue("$cidade", empresa.Endereco.Cidade.Id);
                        cmd.ExecuteScalar();
                        empresa.Endereco.Id = Convert.ToInt32(conn.LastInsertRowId);
                        #endregion

                        cmd.CommandText = "INSERT INTO Empresa (CNPJ, Razao_Social, Nome_Fantasia, Id_Contato, Id_Endereco) " +
                        "VALUES ($cnpj, $razaoSocial, $nomeFantasia, $idContato, $idEndereco);";
                        cmd.Parameters.AddWithValue("$cnpj", empresa.CNPJ);
                        cmd.Parameters.AddWithValue("$razaoSocial", empresa.RazaoSocial);
                        cmd.Parameters.AddWithValue("$nomeFantasia", empresa.NomeFantasia);
                        cmd.Parameters.AddWithValue("$idContato", empresa.Contato.Id);
                        cmd.Parameters.AddWithValue("$idEndereco", empresa.Endereco.Id);
                        await cmd.ExecuteNonQueryAsync();
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

            return true;
        }

        public async Task<DadosEmpresa> ConsultarEmpresaPorId(int empresaId)
        {
            DadosEmpresa empresa = null;
            using (var cmd = _db.GetConnection().CreateCommand())
            {
                cmd.CommandText = @"
                SELECT 
                    em.Id as EmpresaId, 
                    em.Razao_Social, 
                    em.Nome_Fantasia, 
                    co.Id as ContatoId, 
                    co.Email, 
                    co.Nome_Contato,
                    t1.Id as TelPriId,
                    t1.DDD as TelPrilDDD,
                    t1.Numero as TelPriNumero,
                    t2.Id as TelCelId,
                    t2.DDD as TelCelDDD,
                    t2.Numero as TelCelNumero,
                    t3.Id as TelComId,
                    t3.DDD as TelComDDD,
                    t3.Numero as TelComNumero,
                    en.Id as EnderecoId,
                    en.Logradouro,
                    en.Numero,
                    en.Complemento,
                    en.Bairro,
                    en.CEP,
                    cd.Id as CidadeId,
                    cd.Nome as Cidade,
                    es.Id as EstadoId,
                    es.Nome as Estado
                FROM Empresa em
                INNER JOIN Contato co ON em.Id_Contato = co.Id
                INNER JOIN Telefone t1 ON t1.Id = co.Id_Tel_Principal
                INNER JOIN Telefone t2 ON t2.Id = co.Id_Tel_Celular
                INNER JOIN Telefone t3 ON t3.Id = co.Id_Tel_Comercial
                INNER JOIN Endereco en ON en.Id = em.Id_Endereco
                INNER JOIN Cidade cd ON cd.Id = en.Id_Cidade
                INNER JOIN Estado es ON es.Id = cd.Id_Estado
                WHERE em.Id = $empresaId";
                cmd.Parameters.AddWithValue("$empresaId", empresaId);
                var data = await cmd.ExecuteReaderAsync();
                if (data.Read())
                {
                    empresa = new DadosEmpresa();
                    
                    empresa.Id = data.GetInt32(0);
                    empresa.RazaoSocial = data.GetString(1);
                    empresa.NomeFantasia = data.GetString(2);
                    empresa.Contato.Id = data.GetInt32(3);
                    empresa.Contato.Email = data.GetString(4);
                    empresa.Contato.NomeContato = data.GetString(5);
                    empresa.Contato.TelefonePrincipal.Id = data.GetInt32(6);
                    empresa.Contato.TelefonePrincipal.DDD = data.GetInt32(7);
                    empresa.Contato.TelefonePrincipal.NumeroTelefone = data.GetString(8);
                    empresa.Contato.TelefoneCelular.Id = data.GetInt32(9);
                    empresa.Contato.TelefoneCelular.DDD = data.GetInt32(10);
                    empresa.Contato.TelefoneCelular.NumeroTelefone = data.GetString(11);
                    empresa.Contato.TelefoneComercial.Id = data.GetInt32(12);
                    empresa.Contato.TelefoneComercial.DDD = data.GetInt32(13);
                    empresa.Contato.TelefoneComercial.NumeroTelefone = data.GetString(14);
                    empresa.Endereco.Id = data.GetInt32(15);
                    empresa.Endereco.Logradouro = data.GetString(16);
                    empresa.Endereco.NumeroEndereco = data.GetString(17);
                    empresa.Endereco.Complemento = data.GetString(18);
                    empresa.Endereco.Bairro = data.GetString(19);
                    empresa.Endereco.CEP = data.GetString(20);
                    empresa.Endereco.Cidade.Id = data.GetInt32(21);
                    empresa.Endereco.Cidade.NomeCidade = data.GetString(22);
                    empresa.Endereco.Cidade.Estado.Id = data.GetInt32(23);
                    empresa.Endereco.Cidade.Estado.NomeEstado = data.GetString(24);
                }
            }

            return empresa;
        }

        public async Task<bool> AlterarEmpresa(DadosEmpresa empresa)
        {
            using (var conn = _db.GetConnection())
            {
                try
                {
                    using (var cmd = conn.CreateCommand())
                    {
                        #region CONTATO
                        cmd.CommandText = "UPDATE Telefone SET DDD = $ddd, Numero = $numero WHERE Id = $Id;";
                        cmd.Parameters.AddWithValue("$ddd", empresa.Contato.TelefonePrincipal.DDD);
                        cmd.Parameters.AddWithValue("$numero", empresa.Contato.TelefonePrincipal.NumeroTelefone);
                        cmd.Parameters.AddWithValue("$Id", empresa.Contato.TelefonePrincipal.Id);
                        cmd.ExecuteNonQuery();

                        cmd.CommandText = "UPDATE Telefone SET DDD = $ddd, Numero = $numero WHERE Id = $Id;"; 
                        cmd.Parameters.AddWithValue("$ddd", empresa.Contato.TelefoneComercial.DDD);
                        cmd.Parameters.AddWithValue("$numero", empresa.Contato.TelefoneComercial.NumeroTelefone);
                        cmd.Parameters.AddWithValue("$Id", empresa.Contato.TelefoneComercial.Id);
                        cmd.ExecuteNonQuery();

                        cmd.CommandText = "UPDATE Telefone SET DDD = $ddd, Numero = $numero WHERE Id = $Id;"; 
                        cmd.Parameters.AddWithValue("$ddd", empresa.Contato.TelefoneCelular.DDD);
                        cmd.Parameters.AddWithValue("$numero", empresa.Contato.TelefoneCelular.NumeroTelefone);
                        cmd.Parameters.AddWithValue("$Id", empresa.Contato.TelefoneCelular.Id);
                        cmd.ExecuteNonQuery();

                        cmd.CommandText = "UPDATE Contato SET Email = $email, Nome_Contato = $nomeContato WHERE Id = $Id;";
                        cmd.Parameters.AddWithValue("$email", empresa.Contato.Email);
                        cmd.Parameters.AddWithValue("$nomeContato", empresa.Contato.NomeContato);
                        cmd.Parameters.AddWithValue("$Id", empresa.Contato.Id);
                        cmd.ExecuteNonQuery();
                        #endregion

                        #region ENDERECO
                        cmd.CommandText = "UPDATE Endereco SET Logradouro = $logradouro, Numero = $numero, Complemento = $complemento, " +
                            "Bairro = $bairro, CEP = $cep, Id_Cidade = $cidade WHERE Id = $Id;";
                        cmd.Parameters.AddWithValue("$logradouro", empresa.Endereco.Logradouro);
                        cmd.Parameters.AddWithValue("$numero", empresa.Endereco.NumeroEndereco);
                        cmd.Parameters.AddWithValue("$complemento", empresa.Endereco.Complemento);
                        cmd.Parameters.AddWithValue("$bairro", empresa.Endereco.Bairro);
                        cmd.Parameters.AddWithValue("$cep", empresa.Endereco.CEP);
                        cmd.Parameters.AddWithValue("$cidade", empresa.Endereco.Cidade.Id);
                        cmd.Parameters.AddWithValue("$Id", empresa.Endereco.Id);
                        cmd.ExecuteNonQuery();
                        #endregion

                        cmd.CommandText = "UPDATE Empresa SET CNPJ = $cnpj, Razao_Social = $razaoSocial, Nome_Fantasia = $nomeFantasia WHERE Id = $Id;";
                        cmd.Parameters.AddWithValue("$cnpj", empresa.CNPJ);
                        cmd.Parameters.AddWithValue("$razaoSocial", empresa.RazaoSocial);
                        cmd.Parameters.AddWithValue("$nomeFantasia", empresa.NomeFantasia);
                        cmd.Parameters.AddWithValue("$Id", empresa.Id);
                        cmd.ExecuteNonQuery();

                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

            return true;
        }

        public async Task<List<DadosEmpresa>> ListarEmpresas()
        {
            List<DadosEmpresa> empresas = new List<DadosEmpresa>();
            using (var cmd = _db.GetConnection().CreateCommand())
            {
                cmd.CommandText = "SELECT Id, Nome_Fantasia FROM Empresa";
                var data = await cmd.ExecuteReaderAsync();
                while (data.Read())
                {
                    var empresa = new DadosEmpresa();
                    empresa.Id = data.GetInt32(0);
                    empresa.NomeFantasia = data.GetString(1);
                    empresas.Add(empresa);
                }
            }

            return empresas;
        }
    }
}
