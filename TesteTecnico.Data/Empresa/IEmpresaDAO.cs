﻿using TesteTecnico.Dominio.Empresa;

namespace TesteTecnico.Data.Empresa
{
    public interface IEmpresaDAO
    {
        Task<bool> CadastrarEmpresa(DadosEmpresa empresa);
        Task<DadosEmpresa> ConsultarEmpresaPorId(int empresaId);
        Task<bool> AlterarEmpresa(DadosEmpresa empresa);
        Task<List<DadosEmpresa>> ListarEmpresas();
    }
}
