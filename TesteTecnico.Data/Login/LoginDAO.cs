﻿using TesteTecnico.Data.SQLiteDataBase;
using TesteTecnico.Dominio.Usuario;

namespace TesteTecnico.Data.Login
{
    public class LoginDAO : ILoginDAO
    {
        private readonly ISQLiteDB _db;
        public LoginDAO(ISQLiteDB db)
        {
            _db = db;
        }

        public void ListarUsuarios()
        {
            List<UsuarioLogin> usuarios = new List<UsuarioLogin>();
            using (var cmd = _db.GetConnection().CreateCommand())
            {
                cmd.CommandText = "SELECT * FROM Usuario_Login";
                var data = cmd.ExecuteReader();
                while(data.Read())
                {
                    var usuario = new UsuarioLogin();
                    usuario.Usuario = data.GetString(0).ToString();
                    usuario.Senha = data.GetString(1).ToString();
                    usuario.Recuperacao = data.GetString(2).ToString();
                    usuarios.Add(usuario);
                }
            }
        }

        public async Task<UsuarioLogin> ConsultarUsuarioPorLogin(string usuarioLogin)
        {
            UsuarioLogin usuario = new UsuarioLogin();
            using (var cmd = _db.GetConnection().CreateCommand())
            {
                cmd.CommandText = "SELECT * FROM Usuario_Login WHERE Usuario = $usuarioLogin";
                cmd.Parameters.AddWithValue("$usuarioLogin", usuarioLogin);
                var data = await cmd.ExecuteReaderAsync();
                if (data.Read())
                {
                    usuario.Id = data.GetInt32(0);
                    usuario.Usuario = data.GetString(1).ToString();
                    usuario.Senha = data.GetString(2).ToString();
                    usuario.Recuperacao = data.GetString(3).ToString();
                }
            }

            return usuario;
        }

        public async Task<bool> AtualizarSenha(UsuarioLogin usuario)
        {
            using (var cmd = _db.GetConnection().CreateCommand())
            {
                cmd.CommandText = "UPDATE Usuario_Login SET Senha = $senha WHERE Id = $id";
                cmd.Parameters.AddWithValue("$senha", usuario.Senha);
                cmd.Parameters.AddWithValue("$id", usuario.Id);
                var data = await cmd.ExecuteNonQueryAsync();
            }

            return true;
        }
    }
}
