﻿using TesteTecnico.Dominio.Usuario;

namespace TesteTecnico.Data.Login
{
    public interface ILoginDAO
    {
        Task<UsuarioLogin> ConsultarUsuarioPorLogin(string usuarioLogin);
        Task<bool> AtualizarSenha(UsuarioLogin usuario);
    }
}
