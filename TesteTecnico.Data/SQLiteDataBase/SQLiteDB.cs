﻿using System.Data.SQLite;

namespace TesteTecnico.Data.SQLiteDataBase
{
    public class SQLiteDB : ISQLiteDB
    {
        private SQLiteConnection _conn;
        private readonly string _connectionString;

        public SQLiteDB()
        {
            _connectionString = @"DataSource=..\testetecnico.db";
            InitializeDataBase();
        }

        private void InitializeDataBase()
        {
            _conn = new SQLiteConnection();
            SQLiteCommand cmd;
            SQLiteDataReader data;

            try
            {
                using (_conn = GetConnection())
                {
                    var tables = new List<string>();
                    cmd = _conn.CreateCommand();
                    cmd.CommandText = "SELECT name FROM sqlite_master WHERE type = 'table'";
                    data = cmd.ExecuteReader();
                    while(data.Read())
                    {
                        tables.Add(data.GetString(0));
                    }

                    #region TABLES
                    if (!tables.Contains("Usuario_Login"))
                    {
                        cmd = _conn.CreateCommand();
                        cmd.CommandText = "CREATE TABLE Usuario_Login (Id INTEGER PRIMARY KEY, Usuario VARCHAR(40), Senha VARCHAR(10), Email_Recuperacao VARCHAR(256));";
                        cmd.ExecuteNonQuery();
                    }

                    if (!tables.Contains("Empresa"))
                    {
                        cmd = _conn.CreateCommand();
                        cmd.CommandText = "CREATE TABLE Empresa (Id INTEGER PRIMARY KEY, CNPJ VARCHAR(14), Razao_Social VARCHAR(256), Nome_Fantasia VARCHAR(256), Id_Contato INT, Id_Endereco INT);";
                        cmd.ExecuteNonQuery();
                    }

                    if (!tables.Contains("Contato"))
                    {
                        cmd = _conn.CreateCommand();
                        cmd.CommandText = "CREATE TABLE Contato (Id INTEGER PRIMARY KEY, Email VARCHAR(256), Nome_Contato VARCHAR(256), Id_Tel_Principal INT, Id_Tel_Comercial INT, Id_Tel_Celular INT);";
                        cmd.ExecuteNonQuery();
                    }

                    if (!tables.Contains("Telefone"))
                    {
                        cmd = _conn.CreateCommand();
                        cmd.CommandText = "CREATE TABLE Telefone (Id INTEGER PRIMARY KEY, DDD INT, Numero VARCHAR(10));";
                        cmd.ExecuteNonQuery();
                    }

                    if (!tables.Contains("Cidade"))
                    {
                        cmd = _conn.CreateCommand();
                        cmd.CommandText = "CREATE TABLE Cidade (Id INTEGER PRIMARY KEY, Nome VARCHAR(40), Id_Estado INT);";
                        cmd.ExecuteNonQuery();
                    }

                    if (!tables.Contains("Estado"))
                    {
                        cmd = _conn.CreateCommand();
                        cmd.CommandText = "CREATE TABLE Estado (Id INTEGER PRIMARY KEY, Nome VARCHAR(40));";
                        cmd.ExecuteNonQuery();
                    }

                    if (!tables.Contains("Endereco"))
                    {
                        cmd = _conn.CreateCommand();
                        cmd.CommandText = "CREATE TABLE Endereco (Id INTEGER PRIMARY KEY, Logradouro VARCHAR(256), Numero VARCHAR(10), Complemento VARCHAR(40), " +
                            "Bairro VARCHAR(40), CEP VARCHAR(10), Id_Cidade INT);";
                        cmd.ExecuteNonQuery();
                    }
                    #endregion

                    #region INSERTS
                    cmd = _conn.CreateCommand();
                    cmd.CommandText = "SELECT * FROM Usuario_Login";
                    data = cmd.ExecuteReader();
                    if (!data.HasRows)
                    {
                        cmd = _conn.CreateCommand();
                        cmd.CommandText = "INSERT INTO Usuario_Login (Usuario, Senha, Email_Recuperacao) VALUES ('admin', '1234', 'admin@gmail.com');";
                        cmd.ExecuteNonQuery();
                    }

                    cmd = _conn.CreateCommand();
                    cmd.CommandText = "SELECT * FROM Cidade";
                    data = cmd.ExecuteReader();
                    if (!data.HasRows)
                    {
                        cmd = _conn.CreateCommand(); 
                        cmd.CommandText = "INSERT INTO Cidade (Id, Nome, Id_Estado) VALUES (1, 'São Paulo', 1);";
                        cmd.ExecuteNonQuery();
                        cmd.CommandText = "INSERT INTO Cidade (Id, Nome, Id_Estado) VALUES (2, 'Mogi das Cruzes', 1);";
                        cmd.ExecuteNonQuery();
                        cmd.CommandText = "INSERT INTO Cidade (Id, Nome, Id_Estado) VALUES (3, 'Rio de Janeiro', 2);";
                        cmd.ExecuteNonQuery();
                        cmd.CommandText = "INSERT INTO Cidade (Id, Nome, Id_Estado) VALUES (4, 'Belo Horizonte', 3);";
                        cmd.ExecuteNonQuery();
                        cmd.CommandText = "INSERT INTO Cidade (Id, Nome, Id_Estado) VALUES (5, 'Santos', 1);";
                        cmd.ExecuteNonQuery();
                    }

                    cmd = _conn.CreateCommand();
                    cmd.CommandText = "SELECT * FROM Estado";
                    data = cmd.ExecuteReader();
                    if (!data.HasRows)
                    {
                        cmd = _conn.CreateCommand();
                        cmd.CommandText = "INSERT INTO Estado (Id, Nome) VALUES (1, 'SP');";
                        cmd.ExecuteNonQuery();
                        cmd.CommandText = "INSERT INTO Estado (Id, Nome) VALUES (2, 'RJ');";
                        cmd.ExecuteNonQuery();
                        cmd.CommandText = "INSERT INTO Estado (Id, Nome) VALUES (3, 'MG');";
                        cmd.ExecuteNonQuery();
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public SQLiteConnection GetConnection()
        {
            _conn = new SQLiteConnection(_connectionString);
            return _conn.OpenAndReturn();
        }
    }
}