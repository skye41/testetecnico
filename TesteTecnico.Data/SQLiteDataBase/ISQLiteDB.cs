﻿using System.Data.SQLite;

namespace TesteTecnico.Data.SQLiteDataBase
{
    public interface ISQLiteDB
    {
        SQLiteConnection GetConnection();
    }
}
